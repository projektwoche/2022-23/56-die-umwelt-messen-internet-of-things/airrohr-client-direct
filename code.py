import network
import dht
import time
import urequests
from machine import UART
import json
import sds011


def send_data(url, sensor_id, pin1, pin2, data1, data2, software_id):
    header_template = {
        "Content-Type": "application/json",
        "X-Sensor": sensor_id,
    }
    header1 = header_template.copy()
    header1["X-Pin"] = pin1
    header2 = header_template.copy()
    header2["X-Pin"] = pin2
    PM10 = str(data1[0])
    PM25 = str(data1[1])
    temperature = data2[0]
    humidity = data2[1]
    post_data1 = {
        "software_version": software_id,
        "sensordatavalues": [
            {"value_type": "P1", "value": PM10},
            {"value_type": "P2", "value": PM25},
        ],
    }
    post_data2 = {
        "software_version": software_id,
        "sensordatavalues": [
            {"value_type": "temperature", "value": temperature},
            {"value_type": "humidity", "value": humidity},
        ],
    }
    res = urequests.post(url, headers=header1, data=json.dumps(post_data1))
    print(res.text)
    res.close()
    res = urequests.post(url, headers=header2, data=json.dumps(post_data2))
    print(res.text)
    res.close()


# get sensor objects
pm_sensor = UART(0, baudrate=9600)
dht_sensor = dht.DHT22(machine.Pin(setup.secrets.dht22_pin_hw))

while True:
    time.sleep(setup.secrets.api_rate)
    pm_data = sds011.get(pm_sensor)
    pm25 = pm_data[0]  # pm2.5 data
    pm10 = pm_data[1]  # pm10 data
    dht_sensor.measure()
    print(  # debug print data
        "temperature:",
        dht_sensor.temperature(),
        "humidity:",
        dht_sensor.humidity(),
        "pm2.5:",
        pm25,
        "pm10:",
        pm10,
    )
    send_data(
        "https://api.sensor.community/v1/push-sensor-data/",
        setup.secrets.api_esp_id,
        str(
            setup.secrets.sds011_pin_api
        ),  # the SDS011 sensor has a default api pin value of 1
        str(
            setup.secrets.dht22_pin_api
        ),  # the DHT22 sensor has a default api pin value of 7
        (pm10, pm25),
        (dht_sensor.temperature(), dht_sensor.humidity()),
        "kkg_sensor-community-custom-client",
    )
