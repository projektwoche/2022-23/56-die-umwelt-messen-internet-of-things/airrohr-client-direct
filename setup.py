import network
import secrets
import time
import os


def do_connect(wlan, ssid, key):
    if not wlan.isconnected():
        print("connecting to network...")
        wlan.connect(ssid, key)
        while not wlan.isconnected():
            pass
    print("network config:", wlan.ifconfig())
    print("mac adress:", "".join([hex(e)[2:] for e in wlan.config("mac")]).upper())


wlan = network.WLAN(network.STA_IF)
wlan.active(True)

do_connect(wlan, secrets.ssid, secrets.key)
time.sleep(1)  # give time to CTRL-C on UART-0/USB
os.dupterm(None, 1)
