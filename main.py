try:
    import setup

    exec(open("code.py").read())
except KeyboardInterrupt:
    pass
except:
    import machine

    machine.reset()
